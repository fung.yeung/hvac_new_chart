
import { Chiller } from './chiller';

export class ApiInput {
    format?: string;
    dateFrom?: string;
    dateTo?: string;
    chillers!: Array<Chiller>;
    yearsOfOperation?: number;
    dropOffPercent?: number;
}
