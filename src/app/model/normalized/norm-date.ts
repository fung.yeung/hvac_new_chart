import { NormChillerData } from "./norm-chiller-data";

export class NormDate {
    date?: string;
    data?: Array<NormChillerData> = new Array<NormChillerData>();
}