import { NormDate } from "./norm-date";

export class NormChiller {
    name?: string;
    dates: Array<NormDate> = new Array<NormDate>();
}