export class NormChillerData {
    time?: string;
    cop?: number;
    copMfY0?: number;
    copMfYn?: number;
    chillWaterTemp?: number;
    deltCoolLoad?: number;
    deltElecCons?: number;
    deltT?: number;
    elecSave?: number;
    flowrate?: number;
    optFlowrate?: number;
    coolLoad?: number;
    coolLoadCalc?: number;
    loadPerc?: number;
    ambTemp?: number;
}