export interface IBroadcastMessage {
    message: string;
    data: any;
}
