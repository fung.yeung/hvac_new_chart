export class ApiData {
    chiller?: number;
    format?: string;
    dateFrom?: string;
    dateTo?: string;
}