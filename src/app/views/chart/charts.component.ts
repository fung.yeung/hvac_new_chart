import { Component, OnInit, ElementRef, ViewChild, ApplicationRef, ViewRef } from '@angular/core';
import { Router } from '@angular/router';

import { HttpClient, HttpHeaders } from '@angular/common/http';
// import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase/app';

import { ApiService } from './../../service/api.service';
import { GoogleChartService } from './../../service/google-chart.service';

import { Config } from './../../config/config';
import { Chiller } from './../../model/chiller';
import { Chart } from './../../model/chart';
import { ApiInput } from './../../model/api-input';
import { NormDate } from 'src/app/model/normalized/norm-date';
import { NormChiller } from 'src/app/model/normalized/norm-chiller';
import { NormChillerData } from 'src/app/model/normalized/norm-chiller-data';
import { environment } from 'src/environments/environment';
import { IBroadcastMessage } from 'src/app/model/core/broadcast-message.interface';


declare var Plotly: any;
@Component({
    selector: 'charts',
    templateUrl: 'charts.component.html',
})
export class ChartsComponent implements OnInit {
    @ViewChild('chart1_div') chart1!: ElementRef<HTMLElement>;
    @ViewChild('chart2_div') chart2!: ElementRef<HTMLElement>;
    @ViewChild('chart3_div') chart3!: ElementRef<HTMLElement>;
    @ViewChild('chart4_div') chart4!: ElementRef<HTMLElement>;
    @ViewChild('chart5_div') chart5!: ElementRef<HTMLElement>;
    @ViewChild('chart6_div') chart6!: ElementRef<HTMLElement>;
    @ViewChild('chart7_div') chart7!: ElementRef<HTMLElement>;
    @ViewChild('chart8_div') chart8!: ElementRef<HTMLElement>;
    @ViewChild('chart9_div') chart9!: ElementRef<HTMLElement>;
    @ViewChild('chart10_div') chart10!: ElementRef<HTMLElement>;
    @ViewChild('chart11_div') chart11!: ElementRef<HTMLElement>;

    @ViewChild('comboChart1_div') comboChart1!: ElementRef<HTMLElement>;
    @ViewChild('comboChart2_div') comboChart2!: ElementRef<HTMLElement>;
    @ViewChild('combo2Chart1_div') combo2Chart1!: ElementRef<HTMLElement>;
    @ViewChild('combo2Chart2_div') combo2Chart2!: ElementRef<HTMLElement>;
    @ViewChild('combo2Chart3_div') combo2Chart3!: ElementRef<HTMLElement>;
    @ViewChild('combo2Chart4_div') combo2Chart4!: ElementRef<HTMLElement>;
    @ViewChild('combo2Chart5_div') combo2Chart5!: ElementRef<HTMLElement>;


    @ViewChild('chart1_thumb') chart1Thumb!: ElementRef<HTMLElement>;
    @ViewChild('chart2_thumb') chart2Thumb!: ElementRef<HTMLElement>;
    @ViewChild('chart3_thumb') chart3Thumb!: ElementRef<HTMLElement>;
    @ViewChild('chart4_thumb') chart4Thumb!: ElementRef<HTMLElement>;
    @ViewChild('chart5_thumb') chart5Thumb!: ElementRef<HTMLElement>;
    @ViewChild('chart6_thumb') chart6Thumb!: ElementRef<HTMLElement>;
    @ViewChild('chart8_thumb') chart8Thumb!: ElementRef<HTMLElement>;
    @ViewChild('chart9_thumb') chart9Thumb!: ElementRef<HTMLElement>;
    @ViewChild('chart10_thumb') chart10Thumb!: ElementRef<HTMLElement>;
    @ViewChild('chart11_thumb') chart11Thumb!: ElementRef<HTMLElement>;

    @ViewChild('comboChart1_thumb') comboChart1Thumb!: ElementRef<HTMLElement>;
    @ViewChild('comboChart2_thumb') comboChart2Thumb!: ElementRef<HTMLElement>;
    @ViewChild('combo2Chart1_thumb') combo2Chart1Thumb!: ElementRef<HTMLElement>;
    @ViewChild('combo2Chart2_thumb') combo2Chart2Thumb!: ElementRef<HTMLElement>;
    @ViewChild('combo2Chart3_thumb') combo2Chart3Thumb!: ElementRef<HTMLElement>;
    @ViewChild('combo2Chart4_thumb') combo2Chart4Thumb!: ElementRef<HTMLElement>;
    @ViewChild('combo2Chart5_thumb') combo2Chart5Thumb!: ElementRef<HTMLElement>;


    private gLib: any;

    constructor(
        // public readonly auth: AngularFireAuth,
        private readonly router: Router,
        private readonly http: HttpClient,
        private readonly gChart: GoogleChartService,
        private readonly appRef: ApplicationRef,
        public readonly api: ApiService) {
    }

    public chillers: Array<Chiller> = [];
    // this is hardcode since accordingly to user this is a "fixed" setting for sunhouse
    // chillerCombos 1 only used by two chart, the rest use chillerCombos2
    public chillerCombos: Array<String> = ['ACC3', 'ACC1 or ACC2', '(ACC1 or ACC2) + ACC3',
        'ACC1 + ACC2', 'ACC1 + ACC2 + ACC3']
    public chillerCombos2: Array<String> = ['ACC1', 'ACC2', 'ACC3', 'ACC1+ACC2', 'ACC1+ACC3',
        'ACC2+ACC3', 'ACC1+ACC2+ACC3']

    public chartInputs: ApiInput = {
        format: 'JSON',
        dateFrom: '',  // will be populated in initReportDates()
        dateTo: '',
        chillers: new Array<Chiller>()
    }
    public dateFrom: Date = new Date();
    public dateTo: Date = new Date();
    public selectedChillerCount = 1;
    public displayCharts = false;
    public displayVariables = false;
    public googleChartsAvailable = false;
    public displayLink = '';
    public chartData: any;
    public combinationData: any;
    public normalized: Array<NormChiller> = new Array<NormChiller>();
    public chartViews: Array<Array<boolean>> = new Array<Array<boolean>>();
    public comboChartViews: Array<Array<boolean>> = new Array<Array<boolean>>();
    public combo2ChartViews: Array<Array<boolean>> = new Array<Array<boolean>>();
    public comboTemperatures: Array<number> = new Array<number>();
    public displayChartSelectors = false;
    public displayMissingData = false;
    public displayLoading = false;
    public displayApiError = false;
    public display3DChart = false;
    public displayPostError = true;
    public displayCooling = true;
    public displayCOP = true;
    public displayCombo = true;
    public buttons: { [key: string]: boolean } = {
        "displayCOP": true,
        "displayCooling": false,
        "displayCombo": false
    };
    public style_map: { [key: number]: string; } = { //mapping for each combo2Chillers combination to point shape representation
        0: 'point { shape-type: cicle; size: 3}',
        1: 'point { shape-type: square; size: 3}',
        2: 'point { shape-type: triangle; size: 3}',
        3: 'point { shape-type: diamond; size: 3}',
        4: 'point { shape-type: star; size: 3}',
        5: 'point { shape-type: polygon; size: 3}',
        6: 'point { shape-type: star; shape-sides: 3; size: 3}'
    };
    public displayTab = 'general';
    public googleSheetIsCurrent = false;
    public rows3d: Array<Array<number>> = [];
    public totRowd3d = 0;
    public temps3d: Array<number> = [];
    public evaps3d: Array<number> = [];
    public percs3d: Array<number> = [];
    public selectedEvap: number = 0;
    public selected3dUnit: string | undefined;
    public filteredCooling: boolean = false;

    public globalChartRef1: any;
    public globalChartRef2: any;
    public globalChartRef3: any;
    public globalChartRef4: any;
    public globalChartRef5: any;
    public globalChartRef6: any;

    ngOnInit() {
        this.api.logConsole('charts ngOnInit');
        this.api.listenForBroadcastMessage().subscribe(broadcast => {
            this.onBroadcastReceived(broadcast);
        });
        this.initReportDates();
        this.initCharts();
        this.getChillers();
    }
    onBroadcastReceived(broadcast: IBroadcastMessage) {
        switch (broadcast.message) {
            case Config.CastGetChillers:
                break;
            case Config.CastGetChillerData:
                this.refreshedChillerData(this);
                break;
            case Config.CastGet3dData:
                this.refreshedChiller3d(this, broadcast.data);
                break;
            case Config.CastLoggedOut:
                this.api.navigateTo('/', 0);
                break;
        }
    }

    hk_now(time_delta:number=0):string {
        /*helper function to get time-zone aware today's date string
        */
        let options = {
            timeZone: 'Asia/Hong_kong',
            year: 'numeric',
            month: 'numeric',
            day: 'numeric',
        } as const
        var formatter = new Intl.DateTimeFormat([], options);
        let today = new Date()
        if (time_delta) {
            let date_after_delta = today.getDate() + time_delta
            today.setDate(date_after_delta)
        }
        var date_parts = formatter.format(today).split('/')
        let year = date_parts.pop()
        date_parts.unshift(year!)
        return date_parts.join('-')
    }

    initReportDates(): void {
        this.api.logConsole('init report dates');
        let today = new Date();
        // for front-end display
        this.dateFrom.setDate(today.getDate() - 1);
        this.dateTo.setDate(today.getDate());
        // pass to CF for compute
        this.chartInputs['dateFrom'] = this.hk_now(-1)
        this.chartInputs['dateTo'] = this.hk_now()
    }

    // API communication
    getChillers(): void {
        this.api.logConsole('get chillers');
        this.displayLoading = true;
        this.displayPostError = false;
        if (environment.production) {
            this.api.callAPI(Config.GetChillersActionCloud)
                .subscribe(
                    (response: any) => this.onGetChillers(response),
                    (error: any) => {
                        this.api.onApiError(error);
                        this.displayPostError = true;
                        this.displayLoading = false;
                    }
                );
        } else {
            // this.api.callAPI(Config.GetChillersActionCloud)
            this.api.callAPILocal(Config.GetChillersActionLocal)
                .subscribe(
                    (response: any) => this.onGetChillers(response),
                    (error: any) => {
                        this.api.onApiError(error);
                        this.displayPostError = true;
                        this.displayLoading = false;
                        this.displayVariables = false;
                    }
                );
        }
    }
    onGetChillers(data: any): void {
        this.displayLoading = false;
        this.displayVariables = true;
        if (data.data != {}) {
            this.api.logConsole('got chillers', data);
            Object.assign(this.chillers, data.data.chillers);
            // this.chillers = data.data.chillers;
            for (let i = 0; i < this.chillers.length; i++) {
                let chiller = this.chillers[i];
                chiller.name = chiller.id;
                chiller.dropOffPercent = 0.01;
                chiller.operationYears = 2;
                chiller.selected = (i == 0);
            }
            for (let j = 0; j < Config.availableCharts; j++) {
                // for normal chart
                this.chartViews[j] = new Array<boolean>();
                this.chartViews[j].push(true);
                for (let i = 0; i < this.chillers.length; i++) {
                    this.chartViews[j].push(false);
                }
            }
            for (let j = 0; j < Config.availableComboCharts; j++) {
                // for combo chart
                this.comboChartViews[j] = new Array<boolean>();
                this.comboChartViews[j].push(true);
                // let comboChartLength = 
                for (let i = 0; i < this.chillerCombos.length; i++) {
                    this.comboChartViews[j].push(false);
                }
            }
            for (let j = 0; j < Config.availableCombo2Charts; j++) {
                // for combo chart
                this.combo2ChartViews[j] = new Array<boolean>();
                this.combo2ChartViews[j].push(true);
                // let comboChartLength = 
                for (let i = 0; i < this.chillerCombos2.length; i++) {
                    this.combo2ChartViews[j].push(false);
                }
            }
            this.api.logConsole('this.chartViews', this.chartViews);
            this.appRef.tick();
        } else {
            // console.log('failed to retrieve chillers');
            this.displayApiError = true;
        }
    }
    getChillerData(): void {
        this.displayPostError = false;
        this.displayCharts = false;
        this.displayMissingData = false;
        this.displayApiError = false;
        this.displayLoading = true;
        this.displayCOP = true;
        this.displayCooling = true;
        this.displayCombo = true;
        this.chartData = [];
        this.collectToggledChillers();
        this.toggleDataVariables();
        this.api.logConsole('fetch chart data', this.chartInputs);
        this.displayChartSelectors = (this.chartInputs.chillers.length > 1);
        this.googleSheetIsCurrent = true;
        this.api.reloadAuth(this, Config.CastGetChillerData, null);
        // this.refreshFirebaseToken(this.refreshedChillerData, null);
        /*
    if (Config.production) {
        this.api.callAPI(Config.GetChillerDataActionCloud, this.chartInputs)
            .subscribe(
                (response: any) => this.onGetChillerData(response),
                (error: any) => {
                    this.api.onApiError(error);
                    this.displayPostError = true;
                    this.displayLoading = false;
                    this.displayVariables = true;
                }
            );
    } else {
        // this.api.callAPI(Config.GetChillerDataActionCloud, this.chartInputs)
        this.api.callAPILocal(Config.GetChillerDataActionLocal, this.chartInputs)
            .subscribe(
                (response: any) => this.onGetChillerData(response),
                (error: any) => {
                    this.api.onApiError(error);
                    this.displayPostError = true;
                    this.displayLoading = false;
                    this.displayVariables = true;
                }
            );
    }
            */
    }
    refreshedChillerData(ctx: any) {
        if (environment.production) {
            ctx.api.callAPI(Config.GetChillerDataActionCloud, ctx.chartInputs)
                .subscribe(
                    (response: any) => ctx.onGetChillerData(response),
                    (error: any) => {
                        ctx.api.onApiError(error);
                        ctx.displayPostError = true;
                        ctx.displayLoading = false;
                        ctx.displayVariables = true;
                        this.appRef.tick();
                    }
                );
        } else {
            // this.api.callAPI(Config.GetChillerDataActionCloud, this.chartInputs)
            ctx.api.callAPILocal(Config.GetChillerDataActionLocal, ctx.chartInputs)
                .subscribe(
                    (response: any) => ctx.onGetChillerData(response),
                    (error: any) => {
                        ctx.api.onApiError(error);
                        ctx.displayPostError = true;
                        ctx.displayLoading = false;
                        ctx.displayVariables = true;
                        this.appRef.tick();
                    }
                );
        }
    }
    onGetChillerData(data: any): void {
        this.displayLoading = false;
        this.chartData = [];
        this.combinationData = {};
        if (data.data != {}) {
            this.api.logConsole('got chiller data');
            Object.assign(this.chartData, data.data.chillers);
            Object.assign(this.combinationData, data.data.combinations[0]);
            // Object.assign(this.normalized, this.normalizeData(data.data));
            // this.chartData = data.data.chillers;
            // this.api.logConsole(data)
            this.api.logConsole(this.chartData);
            this.api.logConsole(this.combinationData);
            this.displayLink = data.data.file_url;
            this.drawCharts();
            this.appRef.tick();
        } else {
            // console.log('failed to retrieve chillers');
            this.displayApiError = true;
            this.appRef.tick();
        }
    }
    normalizeData(data: any): any {
        let raw = data;
        let normChillers = new Array<NormChiller>();
        for (let i = 0; i < data.chillers.length; i++) {
            // console.log('data.chillers.length', data.chillers);
            let dChiller = data.chillers[i];
            let normChiller = new NormChiller();
            normChiller.name = dChiller.id;
            // console.log('dChiller.name', dChiller.name);
            for (let j = 0; j < Object.keys(dChiller.data[Config.FLDtime]).length; j++) {
                let dRow = dChiller.data;
                let dParts = dRow[Config.FLDtime][j].split(' ');
                let dDate = dParts[0];
                let dTime = dParts[1];
                let normDate: NormDate = this.api.findObjectInArrayByKey(normChiller.dates, 'date', dDate);
                if (normDate == null) {
                    // console.log('didnt find', dDate);
                    normDate = {
                        date: dDate,
                        data: new Array<NormChillerData>()
                    }
                    normChiller.dates.push(normDate);
                }
                let normChillerData: NormChillerData = {
                    time: dTime,
                    cop: dRow[`${normChiller.name} ${Config.FLDcop}`][j],
                    copMfY0: dRow[`${normChiller.name} ${Config.FLDcopMfY0}`][j],
                    copMfYn: dRow[`${normChiller.name} ${Config.FLDcopMfYn}`][j],
                    chillWaterTemp: dRow[`${normChiller.name} ${Config.FLDchillWaterTemp}`][j],
                    deltCoolLoad: dRow[`${normChiller.name} ${Config.FLDdeltCoolLoad}`][j],
                    deltElecCons: dRow[`${normChiller.name} ${Config.FLDdeltElecCons}`][j],
                    deltT: dRow[`${normChiller.name} ${Config.FLDdeltT}`][j],
                    elecSave: dRow[`${normChiller.name} ${Config.FLDelecSave}`][j],
                    flowrate: dRow[`${normChiller.name} ${Config.FLDflowrate}`][j],
                    optFlowrate: dRow[`${normChiller.name} ${Config.FLDoptFlowrate}`][j],
                    coolLoad: dRow[`${normChiller.name} ${Config.FLDcoolLoad}`][j],
                    coolLoadCalc: dRow[`${normChiller.name} ${Config.FLDcoolLoadCalc}`][j],
                    loadPerc: dRow[`${normChiller.name} ${Config.FLDloadPerc}`][j],
                    ambTemp: dRow[`${Config.FLDambTemp}`][j]
                }
                normDate.data?.push(normChillerData);
                /*
                chillers [
                    {
                        name: 'acc1'
                        dates: [
                            {
                                date: '2020-01-01'
                                data: [
                                    {
                                        time: 00:00
                                        cop: 444
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        name: 'acc2'
                    }
                ]
                */
            }
            normChillers.push(normChiller);
        }
        // console.log('normalized', normChillers);
        return normChillers;
    }
    loadChiller3d(selectObject: any): void {
        const chillerName = selectObject.target.value;
        if (chillerName != '0') {
            this.displayPostError = false;
            this.selected3dUnit = chillerName;
            this.displayLoading = true;
            const chiller = this.api.findObjectInArrayByKey(this.chillers, 'name', chillerName);
            this.api.logConsole('load 3d data', chiller);
            this.api.reloadAuth(this, Config.CastGet3dData, chiller);
            // this.refreshFirebaseToken(this.refreshedChiller3d, chiller);
            /*
        if (Config.production) {
            this.api.callAPI(Config.Get3dDataActionCloud, chiller)
                .subscribe(
                    (response: any) => this.onGet3dData(response),
                    (error: any) => {
                        this.api.onApiError(error);
                        this.displayPostError = true;
                        this.displayLoading = false;
                    }
                );
        } else {
            // this.api.callAPI(Config.Get3dDataActionCloud, chiller)
            this.api.callAPILocal(Config.Get3dDataActionLocal, chiller)
                .subscribe(
                    (response: any) => this.onGet3dData(response),
                    (error: any) => {
                        this.api.onApiError(error);
                        this.displayPostError = true;
                        this.displayLoading = false;
                    }
                );
        }
                */
        }
    }
    refreshedChiller3d(ctx: any, chiller: any) {
        if (environment.production) {
            ctx.api.callAPI(Config.Get3dDataActionCloud, chiller)
                .subscribe(
                    (response: any) => ctx.onGet3dData(response),
                    (error: any) => {
                        ctx.api.onApiError(error);
                        ctx.displayPostError = true;
                        ctx.displayLoading = false;
                        this.appRef.tick();
                    }
                );
        } else {
            // this.api.callAPI(Config.Get3dDataActionCloud, chiller)
            ctx.api.callAPILocal(Config.Get3dDataActionLocal, chiller)
                .subscribe(
                    (response: any) => ctx.onGet3dData(response),
                    (error: any) => {
                        ctx.api.onApiError(error);
                        ctx.displayPostError = true;
                        ctx.displayLoading = false;
                        this.appRef.tick();
                    }
                );
        }
    }
    onGet3dData(data: any): void {
        // console.log('this.selected3dUnit', this.selected3dUnit);
        Object.assign(this.rows3d, data.data['3d_data']);
        // this.rows3d = data.data['3d_data'];
        // console.log('rows 3d', this.rows3d);
        for (let i = 1; i < this.rows3d.length; i++) { // skip first element, titles
            let row = this.rows3d[i];
            if (this.temps3d.indexOf(Number(row[0])) < 0) {
                this.temps3d.push(Number(row[0]));
            }
            if (this.evaps3d.indexOf(Number(row[1])) < 0) {
                this.evaps3d.push(Number(row[1]));
            }
            if (this.percs3d.indexOf(Number(row[4])) < 0) {
                this.percs3d.push(Number(row[4]));
            }
        }
        // console.log('temps3d', this.temps3d);
        // console.log('evaps3d', this.evaps3d);
        // console.log('percs3d', this.percs3d);
        this.selectEvapTemp(this.evaps3d[0]);
        this.appRef.tick();
    }
    /*
    refreshFirebaseToken(callback: any, callbackParam: any): void {
        let ctx = this;
        firebase.auth().currentUser?.getIdToken(true).then((firebaseToken: string) => {
            // console.log('firebase token', firebaseToken);
            this.api.saveFirebaseSessionToken(firebaseToken);
            // this.connectHvacLogin(firebaseToken);
            callback(ctx, callbackParam);
        });
    }
    */

    // form control changes
    setFromDate(event: any): void {
        this.dateFrom = new Date(event.target.value);
        this.chartInputs.dateFrom = this.api.getShortDate(this.dateFrom);
        this.googleSheetIsCurrent = false;
    }
    setToDate(event: any): void {
        this.dateTo = new Date(event.target.value);
        this.chartInputs.dateTo = this.api.getShortDate(this.dateTo);
        this.googleSheetIsCurrent = false;
    }
    toggleChiller(chiller: Chiller): void {
        chiller.selected = !chiller.selected;
        this.updateChillerCount();
        this.googleSheetIsCurrent = false;
    }
    toggleDataVariables(): void {
        this.displayVariables = !this.displayVariables;
        this.appRef.tick();
    }
    updateChillerCount(): void {
        let selected: number = 0;
        for (let i = 0; i < this.chillers.length; i++) {
            if (this.chillers[i].selected) {
                selected++;
            }
        }
        this.selectedChillerCount = selected;
        if (selected > 1) {
            this.chartInputs.format = 'JSON';
        }
    }
    collectToggledChillers(): void {
        let selected: Array<any> = [];
        for (let i = 0; i < this.chillers.length; i++) {
            if (this.chillers[i].selected) {
                let chillerData = {
                    id: this.chillers[i].name,
                    name: this.chillers[i].name,
                    dropOffPercent: this.chillers[i].dropOffPercent,
                    operationYears: this.chillers[i].operationYears
                }
                selected.push(chillerData);
            }
        }
        this.chartInputs.chillers = selected;
    }
    setDropPercent(chiller: Chiller, event: any): void {
        chiller.dropOffPercent = Number(event.target.value);
        this.googleSheetIsCurrent = false;
    }
    setOperationYears(chiller: Chiller, event: any): void {
        chiller.operationYears = Number(event.target.value);
        this.googleSheetIsCurrent = false;
    }
    logout() {
        this.api.logout(this);
        /*
        let authState = this.auth.authState;
        authState.subscribe(user => {
            if (!user)  {
                // console.log('logged out properly?');
                this.api.clearTokens();
                this.api.navigateTo('/', 0);
            }
        });
        this.auth.signOut();
        */
    }


    // charting
    initCharts(): void {
        this.api.logConsole('init google charts');
        this.gLib = this.gChart.getGoogle();
        this.gLib.charts.load('current', { 'packages': ['corechart', 'table'] });
        this.gLib.charts.setOnLoadCallback(this.chartsAvailable.bind(this));
    }
    chartsAvailable(): void {
        // console.log('google charts available');
        this.googleChartsAvailable = true;
    }
    drawCharts(): void {
        this.api.logConsole('draw charts');
        if (this.googleChartsAvailable) {
            if (Object.keys(this.chartData[0].data['Time']).length > 0) {
                this.displayCharts = true;
                this.displayCOP = true;
                this.displayCooling = false;
                this.displayCombo = false;
                // must follow drawChart number increment because it is referred from HTML as well as backend function
                this.drawChart1();
                this.drawChart2();
                this.drawChart3();
                this.drawChart4();
                this.drawChart5();
                this.drawChart6();
                this.drawChart8();
                //3.2 To show the cooling load against outdoor temperature
                this.drawChart9();
                //3.7.1 To show the condenser approach temperature over time
                this.drawChart10();
                //3.7.2 To show the evaporator approach temperature over time
                this.drawChart11();
                //3.3 To show different operating chillers against cooling production
                this.drawComboChart1();
                //3.4 To show different operating chillers against outdoor temperature, this is just changing the x axis of 3.3
                this.drawComboChart2();
                //3.5.1 Chilled water supply temperature and outdoor temperature
                this.drawCombo2Chart1();
                //3.5.2 Chilled water supply temperature and cooling load kW
                this.drawCombo2Chart2();
                //3.6 To show the chilled water temperature difference against cooling load
                this.drawCombo2Chart3();
                //3.1.1 Plot 1: Chillers combinations’ COP
                this.drawCombo2Chart4();
                //3.1.2 Plot 2: Chillers and pumps combinations’ COP
                this.drawCombo2Chart5();


                // this.toggleAnalysis('displayCombo') // dev only
            } else {
                // console.log('empty data set');
                this.displayMissingData = true;
                this.displayVariables = true;
            }
        } else {
            // console.log('google charts not available');
        }
    }
    drawChart1(): void {
        this.api.logConsole('draw chart 1');
        const chartIdx = 0;

        let data = new this.gLib.visualization.DataTable();
        data.addColumn('datetime', 'Time');

        let chartTitle = 'COP VS Manufacturer COP: ';
        const chillerCount = this.chartInputs.chillers.length;
        let displayCount = 0;
        for (let i = 0; i < chillerCount; i++) {
            let chillerName = this.chartInputs.chillers[i].name;
            if (this.displayUnit(chartIdx, i)) {
                // console.log('display chart', i + 1);
                data.addColumn('number', `${chillerName} COP`);
                data.addColumn('number', `${chillerName} COP manufacturer Y0`);
                data.addColumn('number', `${chillerName} COP manufacturer Yn`);
                chartTitle += (displayCount++ > 0 ? ', ' : '') + chillerName;
                // console.log('chiller cols', chillerName);
            }
        }

        let chartRows = new Array();
        const rowCount = Object.keys(this.chartData[0].data['Time']).length;
        for (let i = 0; i < rowCount; i++) {
            let cellDate = new Date(this.chartData[0].data['Time'][i]);
            let rowData = new Array();
            rowData.push(cellDate);
            for (let j = 0; j < this.chartInputs.chillers.length; j++) {
                if (this.displayUnit(chartIdx, j)) {
                    let chillerName = this.chartInputs.chillers[j].id;
                    let chartRow = this.chartData[j].data;
                    rowData.push(chartRow[`${chillerName} COP`][i]);
                    rowData.push(chartRow[`${chillerName} COP manufacturer Y0`][i]);
                    rowData.push(chartRow[`${chillerName} COP manufacturer Yn`][i]);
                }
            }
            chartRows.push(rowData);
        }
        data.addRows(chartRows);

        const options = this.getGoogleChartOptions(chartTitle, 'COP', false, true, 'date', false);

        let chart = new this.gLib.visualization.LineChart(this.chart1?.nativeElement);
        this.addChartThumbnail(this.chart1Thumb?.nativeElement, chart);
        chart.draw(data, options);
    }
    drawChart2(): void {
        this.api.logConsole('draw chart 2');
        const chartIdx = 1;

        let temperatures = new Array<number>();
        const rowCount = Object.keys(this.chartData[0].data['Time']).length;
        for (let i = 0; i < rowCount; i++) {
            for (let j = 0; j < this.chartInputs.chillers.length; j++) {
                if (this.displayUnit(chartIdx, j)) {
                    let chartRow = this.chartData[j].data;
                    let roundedAmbient = Math.round(chartRow['Outdoor temperature'][i]);
                    if (!isNaN(roundedAmbient)) {
                        if (temperatures.indexOf(roundedAmbient) < 0) {
                            temperatures.push(roundedAmbient);
                        }
                    }
                }
            }
        }
        temperatures.sort();
        // console.log('temperatures', temperatures);

        let chartTitle = 'Chillers : ';
        const chillerCount = this.chartInputs.chillers.length;
        let displayCount = 0;
        for (let i = 0; i < chillerCount; i++) {
            let chillerName = this.chartInputs.chillers[i].name;
            if (this.displayUnit(chartIdx, i)) {
                chartTitle += (displayCount++ > 0 ? ', ' : '') + chillerName;
            }
        }

        let data = new this.gLib.visualization.DataTable();
        data.addColumn('number', 'Load Percentage');
        for (let i = 0; i < temperatures.length; i++) {
            data.addColumn('number', temperatures[i] + ' C');
        }

        let chartRows = new Array();
        // const rowCount = Object.keys(this.chartData[0].data['Time']).length;
        for (let i = 0; i < rowCount; i++) {
            let cellDate = new Date(this.chartData[0].data['Time'][i]);
            let rowData = new Array();
            rowData.push(cellDate);
            for (let j = 0; j < this.chartInputs.chillers.length; j++) {
                if (this.displayUnit(chartIdx, j)) {
                    let chillerName = this.chartInputs.chillers[j].id;
                    let chartRow = this.chartData[j].data;
                    let roundedAmbient = Math.round(chartRow['Outdoor temperature'][i]);
                    if (!isNaN(roundedAmbient)) {
                        let column = temperatures.indexOf(roundedAmbient);
                        let rowData = new Array<any>();
                        rowData.push(chartRow[`${chillerName} load percentage`][i] / 100);
                        for (let j = 0; j < temperatures.length; j++) {
                            rowData.push(
                                column == j ? chartRow[`${chillerName} COP`][i] : null
                            );
                        }
                        chartRows.push(rowData);
                    }
                }
            }
        }
        data.addRows(chartRows);

        const options = this.getGoogleChartOptions(chartTitle, 'COP', true, true, 'percent', true);

        let chart = new this.gLib.visualization.ScatterChart(this.chart2?.nativeElement);
        this.addChartThumbnail(this.chart2Thumb?.nativeElement, chart);
        chart.draw(data, options);
    }
    drawChart3(): void {
        this.api.logConsole('draw chart 3');
        const chartIdx = 2;

        let temperatures = new Array<number>();
        const rowCount = Object.keys(this.chartData[0].data['Time']).length;
        for (let i = 0; i < rowCount; i++) {
            for (let j = 0; j < this.chartInputs.chillers.length; j++) {
                if (this.displayUnit(chartIdx, j)) {
                    let chartRow = this.chartData[j].data;
                    let roundedAmbient = Math.round(chartRow['Outdoor temperature'][i]);
                    if (!isNaN(roundedAmbient)) {
                        if (temperatures.indexOf(roundedAmbient) < 0) {
                            temperatures.push(roundedAmbient);
                        }
                    }
                }
            }

        }
        temperatures.sort();
        // console.log('temperatures', temperatures);

        let chartTitle = 'Chillers : ';
        const chillerCount = this.chartInputs.chillers.length;
        let displayCount = 0;
        for (let i = 0; i < chillerCount; i++) {
            let chillerName = this.chartInputs.chillers[i].name;
            if (this.displayUnit(chartIdx, i)) {
                chartTitle += (displayCount++ > 0 ? ', ' : '') + chillerName;
            }
        }

        let data = new this.gLib.visualization.DataTable();
        data.addColumn('number', 'Load Percentage');
        for (let i = 0; i < temperatures.length; i++) {
            data.addColumn('number', temperatures[i] + ' C');
        }

        let chartRows = new Array();
        // const rowCount = Object.keys(this.chartData[0].data['Time']).length;
        for (let i = 0; i < rowCount; i++) {
            let cellDate = new Date(this.chartData[0].data['Time'][i]);
            let rowData = new Array();
            rowData.push(cellDate);
            for (let j = 0; j < this.chartInputs.chillers.length; j++) {
                if (this.displayUnit(chartIdx, j)) {
                    let chillerName = this.chartInputs.chillers[j].id;
                    let chartRow = this.chartData[j].data;
                    let roundedAmbient = Math.round(chartRow['Outdoor temperature'][i]);
                    let column = temperatures.indexOf(roundedAmbient);
                    if (!isNaN(roundedAmbient)) {
                        let rowData = new Array<any>();
                        rowData.push(chartRow[`${chillerName} load percentage`][i] / 100);
                        for (let j = 0; j < temperatures.length; j++) {
                            rowData.push(
                                column == j ? chartRow[`${chillerName} COP manufacturer Yn`][i] : null
                            );
                        }
                        chartRows.push(rowData);
                    }
                }
            }
        }
        data.addRows(chartRows);

        const options = this.getGoogleChartOptions(chartTitle, 'COP Manufacturer', true, true, 'percent', true);

        let chart = new this.gLib.visualization.ScatterChart(this.chart3?.nativeElement);
        this.addChartThumbnail(this.chart3Thumb?.nativeElement, chart);
        chart.draw(data, options);
    }
    drawChart8(): void {
        this.api.logConsole('draw chart 8');
        const chartIdx = 7;

        let temperatures = new Array<number>();
        const rowCount = Object.keys(this.chartData[0].data['Time']).length;
        for (let i = 0; i < rowCount; i++) {
            for (let j = 0; j < this.chartInputs.chillers.length; j++) {
                if (this.displayUnit(chartIdx, j)) {
                    let chartRow = this.chartData[j].data;
                    let roundedAmbient = Math.round(chartRow['Outdoor temperature'][i]);
                    if (!isNaN(roundedAmbient)) {
                        if (temperatures.indexOf(roundedAmbient) < 0) {
                            temperatures.push(roundedAmbient);
                        }
                    }
                }
            }

        }
        temperatures.sort();
        // console.log('temperatures', temperatures);

        let chartTitle = 'Chillers : ';
        const chillerCount = this.chartInputs.chillers.length;
        let displayCount = 0;
        for (let i = 0; i < chillerCount; i++) {
            let chillerName = this.chartInputs.chillers[i].name;
            if (this.displayUnit(chartIdx, i)) {
                chartTitle += (displayCount++ > 0 ? ', ' : '') + chillerName;
            }
        }

        let data = new this.gLib.visualization.DataTable();
        data.addColumn('number', 'Load Percentage');
        for (let i = 0; i < temperatures.length; i++) {
            data.addColumn('number', temperatures[i] + ' C');
        }

        let chartRows = new Array();
        // const rowCount = Object.keys(this.chartData[0].data['Time']).length;
        for (let i = 0; i < rowCount; i++) {
            let cellDate = new Date(this.chartData[0].data['Time'][i]);
            let rowData = new Array();
            rowData.push(cellDate);
            for (let j = 0; j < this.chartInputs.chillers.length; j++) {
                if (this.displayUnit(chartIdx, j)) {
                    let chillerName = this.chartInputs.chillers[j].id;
                    let chartRow = this.chartData[j].data;
                    let roundedAmbient = Math.round(chartRow['Outdoor temperature'][i]);
                    let column = temperatures.indexOf(roundedAmbient);
                    if (!isNaN(roundedAmbient)) {
                        let rowData = new Array<any>();
                        rowData.push(chartRow[`${chillerName} load percentage`][i] / 100);
                        for (let j = 0; j < temperatures.length; j++) {
                            rowData.push(
                                column == j ? chartRow[`${chillerName} Delta T`][i] : null
                            );
                        }
                        chartRows.push(rowData);
                    }
                }
            }
        }
        data.addRows(chartRows);

        const options = this.getGoogleChartOptions(chartTitle, 'Delta T', true, true, 'percent', true);

        let chart = new this.gLib.visualization.ScatterChart(this.chart8?.nativeElement);
        this.addChartThumbnail(this.chart8Thumb?.nativeElement, chart);
        chart.draw(data, options);
    }
    drawChart9(): void {
        this.api.logConsole('draw chart 9: cooling load against outdoor temperature'); //3.2
        const chartIdx = 8;
        const chillerCount = this.chartInputs.chillers.length;
        let data = new this.gLib.visualization.DataTable();
        // group Y-axis by selected chiller
        data.addColumn('number', 'Outdoor temperature');
        for (let i = 0; i < chillerCount; i++) {
            data.addColumn('number', this.chartInputs.chillers[i]['name']);
        }
        let chartRows = new Array();
        const rowCount = Object.keys(this.chartData[0].data['Time']).length;
        for (let i = 0; i < rowCount; i++) { // for each time record
            for (let j = 0; j < chillerCount; j++) { // for each chiller
                if (this.displayUnit(chartIdx, j)) {
                    let chartRow = this.chartData[j].data;
                    //dynamically create empty array base on chiller count + 1
                    //first field for x-axis, the rest is Y-axis value base on group / series
                    // in this example the array is [<outdoor temperature>, <ACC1 Main cooling load kw'>, ...up to N ACC]
                    let rowData = new Array(chillerCount + 1).fill(null);
                    rowData[0] = chartRow['Outdoor temperature'][i];
                    rowData[j + 1] = this.combinationData['Main cooling load kW'][i];
                    // rowData[j + 1] = chartRow['Main cooling load kW'][i];
                    chartRows.push(rowData);
                }
            }
        }
        data.addRows(chartRows);
        let hAxisConf = {
            title: 'Outdoor temperature °C',
            format: '#.#',
            // minValue: 0,
            // maxValue: 1,
            titleTextStyle: {
                fontSize: 14,
                italic: false
            }
        }


        let chartTitle = 'Cooling Load kW vs outdoor temp °C'
        const options = this.getGoogleChartOptions(chartTitle, 'Cooling load kW'
            , true, false, 'refer to hAxisConf Object', true, hAxisConf);

        let chart = new this.gLib.visualization.ScatterChart(this.chart9?.nativeElement);
        this.addChartThumbnail(this.chart9Thumb?.nativeElement, chart);
        chart.draw(data, options);
    }

    drawChart10(): void {
        this.api.logConsole('draw chart 10: 3.7.1 condenser approach temperature over time');
        const chartIdx = 9;

        let data = new this.gLib.visualization.DataTable();
        data.addColumn('datetime', 'Time');

        let chartTitle = 'Condenser Approach Temperature: ';
        const chillerCount = this.chartInputs.chillers.length;
        let displayCount = 0;
        for (let i = 0; i < chillerCount; i++) {
            let chillerName = this.chartInputs.chillers[i].name;
            if (this.displayUnit(chartIdx, i)) {
                // console.log('display chart', i + 1);
                data.addColumn('number', `${chillerName} Condenser Approach Temperature`);
                chartTitle += (displayCount++ > 0 ? ', ' : '') + chillerName;
                // console.log('chiller cols', chillerName);
            }
        }

        let chartRows = new Array();
        const rowCount = Object.keys(this.chartData[0].data['Time']).length;
        for (let i = 0; i < rowCount; i++) {
            let cellDate = new Date(this.chartData[0].data['Time'][i]);
            let rowData = new Array();
            rowData.push(cellDate);
            for (let j = 0; j < this.chartInputs.chillers.length; j++) {
                if (this.displayUnit(chartIdx, j)) {
                    let chillerName = this.chartInputs.chillers[j].id;
                    let chartRow = this.chartData[j].data;
                    rowData.push(chartRow[`${chillerName} Condenser Approach Temperature`][i]);
                }
            }
            chartRows.push(rowData);
        }
        data.addRows(chartRows);

        const options = this.getGoogleChartOptions(chartTitle, 'Condenser Approach Temperature', false, true, 'date', false);

        let chart = new this.gLib.visualization.LineChart(this.chart10?.nativeElement);
        this.addChartThumbnail(this.chart10Thumb?.nativeElement, chart);
        chart.draw(data, options);
    }

    drawChart11(): void {
        this.api.logConsole('draw chart 11: 3.7.2 evaporator approach temperature over time');
        const chartIdx = 10;

        let data = new this.gLib.visualization.DataTable();
        data.addColumn('datetime', 'Time');

        let chartTitle = 'Evaporator Approach Temperature: ';
        const chillerCount = this.chartInputs.chillers.length;
        let displayCount = 0;
        for (let i = 0; i < chillerCount; i++) {
            let chillerName = this.chartInputs.chillers[i].name;
            if (this.displayUnit(chartIdx, i)) {
                // console.log('display chart', i + 1);
                data.addColumn('number', `${chillerName} Evaporator Approach Temperature`);
                chartTitle += (displayCount++ > 0 ? ', ' : '') + chillerName;
                // console.log('chiller cols', chillerName);
            }
        }

        let chartRows = new Array();
        const rowCount = Object.keys(this.chartData[0].data['Time']).length;
        for (let i = 0; i < rowCount; i++) {
            let cellDate = new Date(this.chartData[0].data['Time'][i]);
            let rowData = new Array();
            rowData.push(cellDate);
            for (let j = 0; j < this.chartInputs.chillers.length; j++) {
                if (this.displayUnit(chartIdx, j)) {
                    let chillerName = this.chartInputs.chillers[j].id;
                    let chartRow = this.chartData[j].data;
                    rowData.push(chartRow[`${chillerName} Evaporator Approach Temperature`][i]);
                }
            }
            chartRows.push(rowData);
        }
        data.addRows(chartRows);

        const options = this.getGoogleChartOptions(chartTitle, 'Evaporator Approach Temperature', false, true, 'date', false);

        let chart = new this.gLib.visualization.LineChart(this.chart11?.nativeElement);
        this.addChartThumbnail(this.chart11Thumb?.nativeElement, chart);
        chart.draw(data, options);
    }

    drawComboChart1(): void {
        let chartTitle = 'No. of Chiller vs Cooling Load kW'
        this.api.logConsole(`draw ComboChart 1: ${chartTitle}`); //3.3
        const comboChartidx = 0;

        let data = new this.gLib.visualization.DataTable();
        // dyanmically create column base on user selected combo
        data.addColumn('number', 'Main cooling load kW');
        for (let i = 0; i < this.chillerCombos.length; i++) {
            data.addColumn('number', this.chillerCombos[i])
        }

        let chartRows = new Array();
        let chartRow = this.combinationData;
        let cv_length = this.comboChartViews[comboChartidx].length
        const rowCount = Object.keys(this.combinationData['Time']).length;
        // console.log(`data_len: ${data_len}`)
        // console.log(this.comboChartViews[comboChartidx])
        // // console.log(this.comboChartViews[0][1])
        // console.log(this.displayComboUnit(0, 1))

        for (let i = 0; i < rowCount; i++) { // for each time record
            // for (let i = 0; i < 101; i++) { // for each time record
            for (let j = 0; j < cv_length; j++) {  //for each combination
                //         // console.log(`displayComboUnit, i=${i}, j=${j}, ${this.displayComboUnit(comboChartidx, j)}`)
                if (this.displayComboUnit(comboChartidx, j)) { //if j is 0, run for all combo, if j is > 1, then run only for that combo
                    let cid = chartRow['Chillers combination ID'][i]; // 0- 5. we only want 1 - 5
                    if (cid === j) {
                        //ACC3, k= 0, max 4
                        // console.log(j)

                        //             // console.log(`Display Combo Unit for j = ${j}: ${this.displayComboUnit(comboChartidx, j)}`)
                        let rowData = new Array(cv_length).fill(null);
                        rowData[0] = chartRow['Main cooling load kW'][i];
                        rowData[cid] = cid
                        chartRows.push(rowData);
                    }


                    // //             console.log(`cid value is ${cid}`)
                    //             let target_index = this.comboChartViews[comboChartidx].indexOf(true)
                    // for (let k = 1; k < this.comboChartViews[comboChartidx].length; k++) {
                    //                 console.log(`i value is ${i}`)
                    //                 console.log(`j value is ${j}`)
                    //                 console.log(`cid value is ${cid}`)

                    // rowData.push(
                    //     cid == j?  cid : null
                    // );
                    // }

                    //             // let rowData = new Array(6).fill(null);
                    //             // rowData[0] = chartRow['Main cooling load kW'][i];
                    //             // assign chiller id into different position in array (to group them by series)
                    //             // , value is also chiller id because this is required to display on the chart
                    //             // rowData[cid] = cid;
                    //             // console.log(rowData)
                    //             
                }
            }
            //     // console.log(rowData)
        }
        // console.log(chartRows)        
        data.addRows(chartRows);
        let hAxisConf = {
            title: 'Cooling Load kW',
            format: '#.#',
            titleTextStyle: {
                fontSize: 14,
                italic: false
            }
        }
        const options = this.getGoogleChartOptions(chartTitle, 'Chiller combination'
            , true, false, 'refer to hAxisConf Object', true, hAxisConf);

        let chart = new this.gLib.visualization.ScatterChart(this.comboChart1?.nativeElement);
        this.addChartThumbnail(this.comboChart1Thumb?.nativeElement, chart);
        chart.draw(data, options);
    };

    drawComboChart2(): void {
        let chartTitle = 'No. of Chiller vs Outdoor temperature'
        this.api.logConsole(`draw ComboChart 2: ${chartTitle}`); //3.4
        const comboChartidx = 1;

        let data = new this.gLib.visualization.DataTable();
        // dyanmically create column base on user selected combo
        data.addColumn('number', 'Outdoor temperature');
        for (let i = 0; i < this.chillerCombos.length; i++) {
            data.addColumn('number', this.chillerCombos[i])
        }

        let chartRows = new Array();
        let chartRow = this.combinationData;
        let cv_length = this.comboChartViews[comboChartidx].length
        const rowCount = Object.keys(this.combinationData['Time']).length;

        for (let i = 0; i < rowCount; i++) { // for each time record
            for (let j = 0; j < cv_length; j++) {  //for each combination
                if (this.displayComboUnit(comboChartidx, j)) { //if j is 0, run for all combo, if j is > 1, then run only for that combo
                    let cid = chartRow['Chillers combination ID'][i]; // 0- 5. we only want 1 - 5
                    if (cid === j) {
                        let rowData = new Array(cv_length).fill(null);
                        rowData[0] = chartRow['Outdoor temperature'][i];
                        rowData[cid] = cid
                        chartRows.push(rowData);
                    }
                }
            }
        }
        // console.log(chartRows)

        data.addRows(chartRows);
        let hAxisConf = {
            title: 'Outdoor temperature',
            format: '#.#',
            titleTextStyle: {
                fontSize: 14,
                italic: false
            }
        }
        const options = this.getGoogleChartOptions(chartTitle, 'Chiller combination'
            , true, false, 'refer to hAxisConf Object', true, hAxisConf);

        let chart = new this.gLib.visualization.ScatterChart(this.comboChart2?.nativeElement);
        this.addChartThumbnail(this.comboChart2Thumb?.nativeElement, chart);
        chart.draw(data, options);
    };

    drawCombo2Chart1(): void {  //3.5.1
        let chartTitle = 'CHWS vs outdoor temperature °C'
        this.api.logConsole(`draw Combo2Chart 1: ${chartTitle}`);
        const comboChartidx = 0;

        let data = new this.gLib.visualization.DataTable();
        // dyanmically create column base on user selected combo
        data.addColumn('number', 'Outdoor temperature');
        for (let i = 0; i < this.chillerCombos2.length; i++) {
            data.addColumn('number', `${this.chillerCombos2[i]} Main Evap`)
        }
        let chartRows = new Array();
        let chartRow = this.combinationData;
        let cv_length = this.combo2ChartViews[comboChartidx].length
        const rowCount = Object.keys(this.combinationData['Time']).length;

        for (let i = 0; i < rowCount; i++) { // for each time record
            for (let j = 0; j < cv_length; j++) {  //for each combination
                if (this.displayComboUnit2(comboChartidx, j)) { //if j is 0, run for all combo, if j is > 1, then run only for that combo
                    for (let k = 0; k < this.chillerCombos2.length; k++) {  //get the right combo name, handle the index diff between chartview and actual combo array
                        if (k === j - 1) {
                            let combo_name = `${this.chillerCombos2[k]} Main Evap`;
                            // console.log(`j is ${j}`)
                            // console.log(`k is ${k}`)
                            // console.log(`i is ${i}`)
                            // console.log(`combo_name is ${combo_name}`)
                            let target = chartRow[combo_name][i];
                            // console.log(chart_row)
                            let rowData = new Array(cv_length).fill(null);
                            rowData[0] = chartRow['Outdoor temperature'][i];
                            rowData[k + 1] = target
                            chartRows.push(rowData);
                        }
                    }



                    // let rowData = new Array(cv_length).fill(null);
                    // rowData[0] = chartRow['Outdoor temperature'][i];
                    // if (cid === j) {
                    //     let rowData = new Array(cv_length).fill(null);
                    //     rowData[0] = chartRow['Outdoor temperature'][i];
                    //     rowData[cid] = cid
                    //     chartRows.push(rowData);
                    // }
                }

            }
            // console.log(chartRows)
            // break
        }

        data.addRows(chartRows);
        let hAxisConf = {
            title: 'Outdoor temperature',
            format: '#.#',
            titleTextStyle: {
                fontSize: 14,
                italic: false
            }
        }
        const options = this.getGoogleChartOptions(chartTitle, 'Main Chilled Water Supply temperature'
            , true, false, 'refer to hAxisConf Object', true, hAxisConf);

        let chart = new this.gLib.visualization.ScatterChart(this.combo2Chart1?.nativeElement);
        this.addChartThumbnail(this.combo2Chart1Thumb?.nativeElement, chart);
        chart.draw(data, options);
    };

    drawCombo2Chart2(): void {  //3.5.2
        let chartTitle = 'CHWS vs cooling load kW'
        this.api.logConsole(`draw Combo2Chart 2: ${chartTitle}`);
        const comboChartidx = 1;

        let data = new this.gLib.visualization.DataTable();
        // dyanmically create column base on user selected combo
        data.addColumn('number', 'cooling load kW');
        for (let i = 0; i < this.chillerCombos2.length; i++) {
            data.addColumn('number', `${this.chillerCombos2[i]} Main Evap`)
        }
        let chartRows = new Array();
        let chartRow = this.combinationData;
        let cv_length = this.combo2ChartViews[comboChartidx].length
        const rowCount = Object.keys(this.combinationData['Time']).length;

        for (let i = 0; i < rowCount; i++) { // for each time record
            for (let j = 0; j < cv_length; j++) {  //for each combination
                if (this.displayComboUnit2(comboChartidx, j)) { //if j is 0, run for all combo, if j is > 1, then run only for that combo
                    for (let k = 0; k < this.chillerCombos2.length; k++) {  //get the right combo name, handle the index diff between chartview and actual combo array
                        if (k === j - 1) {
                            let combo_name = `${this.chillerCombos2[k]} Main Evap`;
                            // console.log(`j is ${j}`)
                            // console.log(`k is ${k}`)
                            // console.log(`i is ${i}`)
                            // console.log(`combo_name is ${combo_name}`)
                            let target = chartRow[combo_name][i];
                            // console.log(chart_row)
                            let rowData = new Array(cv_length).fill(null);
                            rowData[0] = chartRow['Main cooling load kW'][i];
                            rowData[k + 1] = target
                            chartRows.push(rowData);
                        }
                    }
                }
            }
            // console.log(chartRows)
            // break
        }

        data.addRows(chartRows);
        let hAxisConf = {
            title: 'Main cooling load kW',
            format: '#.#',
            titleTextStyle: {
                fontSize: 14,
                italic: false
            }
        }
        const options = this.getGoogleChartOptions(chartTitle, 'Main Chilled Water Supply temperature'
            , true, false, 'refer to hAxisConf Object', true, hAxisConf);

        let chart = new this.gLib.visualization.ScatterChart(this.combo2Chart2?.nativeElement);
        this.addChartThumbnail(this.combo2Chart2Thumb?.nativeElement, chart);
        chart.draw(data, options);
    };

    drawCombo2Chart3(): void {  //3.6
        let chartTitle = 'Chilled water delta temp °C vs Cooling Load kW'
        this.api.logConsole(`draw Combo2Chart 3: ${chartTitle}`);
        const comboChartidx = 2;

        let data = new this.gLib.visualization.DataTable();
        // dyanmically create column base on user selected combo
        data.addColumn('number', 'cooling load kW');
        for (let i = 0; i < this.chillerCombos2.length; i++) {
            data.addColumn('number', `${this.chillerCombos2[i]} Main Delta T`)
        }
        let chartRows = new Array();
        let chartRow = this.combinationData;
        let cv_length = this.combo2ChartViews[comboChartidx].length
        const rowCount = Object.keys(this.combinationData['Time']).length;

        for (let i = 0; i < rowCount; i++) { // for each time record
            for (let j = 0; j < cv_length; j++) {  //for each combination
                if (this.displayComboUnit2(comboChartidx, j)) { //if j is 0, run for all combo, if j is > 1, then run only for that combo
                    for (let k = 0; k < this.chillerCombos2.length; k++) {  //get the right combo name, handle the index diff between chartview and actual combo array
                        if (k === j - 1) {
                            let combo_name = `${this.chillerCombos2[k]} Main Delta T`;
                            // console.log(`j is ${j}`)
                            // console.log(`k is ${k}`)
                            // console.log(`i is ${i}`)
                            // console.log(`combo_name is ${combo_name}`)
                            let target = chartRow[combo_name][i];
                            // console.log(chart_row)
                            let rowData = new Array(cv_length).fill(null);
                            rowData[0] = chartRow['Main cooling load kW'][i];
                            rowData[k + 1] = target
                            chartRows.push(rowData);
                        }
                    }
                }
            }
            // console.log(chartRows)
            // break
        }

        data.addRows(chartRows);
        let hAxisConf = {
            title: 'Main cooling load kW',
            format: '#.#',
            titleTextStyle: {
                fontSize: 14,
                italic: false
            }
        }
        const options = this.getGoogleChartOptions(chartTitle, 'Chilled water delta temp °C'
            , true, false, 'refer to hAxisConf Object', true, hAxisConf);

        let chart = new this.gLib.visualization.ScatterChart(this.combo2Chart3?.nativeElement);
        this.addChartThumbnail(this.combo2Chart3Thumb?.nativeElement, chart);
        chart.draw(data, options);
    };

    drawCombo2Chart4(): void {  //3.1.1
        let chartTitle = 'Chillers combinations’ COP'
        this.api.logConsole(`draw Combo2Chart 4: ${chartTitle}`);
        const comboChartidx = 3;

        let data = new this.gLib.visualization.DataTable();

        // Create a sorted, distinct ambient temperature array for
        // categorizing each chiller into group
        // store output in class for reuse when redrawing
        const rowCount = Object.keys(this.combinationData['Time']).length;

        let temperatures: Array<number>
        if (this.comboTemperatures.length > 0) {
            temperatures = this.comboTemperatures
        } else {
            let rounded_temp = new Array<number>();
            let raw = this.combinationData['Outdoor temperature'];
            // unpack rounded temperature into Array
            for (let i = 0; i < rowCount; i++) {
                rounded_temp.push(Math.round(raw[i]))
            }
            // sort distinct tempratures
            let deduplicate = rounded_temp.filter((n, i) => rounded_temp.indexOf(n) === i)
            temperatures = deduplicate.sort((a, b) => a - b)
            this.comboTemperatures = temperatures
            // console.log(temperatures)
        }
        // dyanmically create column base on user selected combo
        data.addColumn('number', 'cooling load kW');
        for (let i = 0; i < temperatures.length; i++) {
            data.addColumn('number', `${temperatures[i]} C`)
            data.addColumn({ 'type': 'string', 'role': 'style' });
            data.addColumn({ 'type': 'string', 'role': 'tooltip', 'p': { 'html': true } });
        };
        // console.log(data)
        // enable style per combination
        let chartRows = new Array();
        let chartRow = this.combinationData;
        let cv_length = this.combo2ChartViews[comboChartidx].length


        // for (let i = 0; i < 10; i++) { // for each time record
        for (let i = 0; i < rowCount; i++) { // for each time record
            for (let j = 0; j < cv_length; j++) {  //for each combination
                if (this.displayComboUnit2(comboChartidx, j)) { //if j is 0, run for all combo, if j is > 1, then run only for that combo
                    for (let k = 0; k < this.chillerCombos2.length; k++) {  //get the right combo name, handle the index diff between chartview and actual combo array
                        if (k === j - 1) {
                            let chiller_cop = `${this.chillerCombos2[k]} COP*`;
                            let curr_row_temp = Math.round(chartRow['Outdoor temperature'][i])
                            let target_temp_index = temperatures.indexOf(curr_row_temp)
                            // console.log(`j is ${j}`)
                            // console.log(`k is ${k}`)
                            // console.log(`i is ${i}`)
                            // console.log(`chiller_cop is ${chiller_cop}`)
                            // console.log(`curr_row_temp is ${curr_row_temp}`)
                            // console.log(`target_temp_index is ${target_temp_index}`)

                            let target = chartRow[chiller_cop][i];
                            let rowData = new Array(data.getNumberOfColumns()).fill(null);
                            rowData[0] = chartRow['Main cooling load kW'][i];

                            // rebuild tooltips content, it will show point's chiller combination instead of the series (tempreature)
                            let tooltip_number = '<br>' + Math.round((chartRow['Main cooling load kW'][i]) * 100) / 100 + ', ' + Math.round(target * 100) / 100
                            // if condition to prevent replacing the rowData[0] data
                            // also assign style to the points base on chiller combination (represented by k)
                            // assign new tooltip
                            let rowData_idx: number = (target_temp_index === 0) ? 1 : (target_temp_index * 3) + 1
                            rowData[rowData_idx] = target
                            rowData[rowData_idx + 1] = this.style_map[k]
                            rowData[rowData_idx + 2] = this.chillerCombos2[k] + tooltip_number
                            chartRows.push(rowData);
                            // console.log(chartRows)
                        }
                    }
                }
            }
            // console.log(chartRows)
            // break
        }

        data.addRows(chartRows);
        // console.log(data)
        let hAxisConf = {
            title: 'Main cooling load kW',
            format: '#.#',
            titleTextStyle: {
                fontSize: 14,
                italic: false
            }
        }
        const options = this.getGoogleChartOptions(chartTitle, 'COP*'
            , true, true, 'refer to hAxisConf Object', true, hAxisConf);

        options['tooltip'] = { isHtml: true }  // required just for editing tooltip content

        let chart = new this.gLib.visualization.ScatterChart(this.combo2Chart4?.nativeElement);
        this.addChartThumbnail(this.combo2Chart4Thumb?.nativeElement, chart);
        chart.draw(data, options);
    };

    drawCombo2Chart5(): void {  //3.1.2
        let chartTitle = 'Chillers combinations’ COP (includes pump)'
        this.api.logConsole(`draw Combo2Chart 5: ${chartTitle}`);
        const comboChartidx = 4;

        let data = new this.gLib.visualization.DataTable();

        // Create a sorted, distinct ambient temperature array for
        // categorizing each chiller into group
        // store output in class for reuse when redrawing
        const rowCount = Object.keys(this.combinationData['Time']).length;

        let temperatures: Array<number>
        if (this.comboTemperatures.length > 0) {
            temperatures = this.comboTemperatures
        } else {
            let rounded_temp = new Array<number>();
            let raw = this.combinationData['Outdoor temperature'];
            // unpack rounded temperature into Array
            for (let i = 0; i < rowCount; i++) {
                rounded_temp.push(Math.round(raw[i]))
            }
            // sort distinct tempratures
            let deduplicate = rounded_temp.filter((n, i) => rounded_temp.indexOf(n) === i)
            temperatures = deduplicate.sort((a, b) => a - b)
            this.comboTemperatures = temperatures
            // console.log(temperatures)
        }
        // dyanmically create column base on user selected combo
        data.addColumn('number', 'cooling load kW');
        for (let i = 0; i < temperatures.length; i++) {
            data.addColumn('number', `${temperatures[i]} C`)
            data.addColumn({ 'type': 'string', 'role': 'style' });
            data.addColumn({ 'type': 'string', 'role': 'tooltip', 'p': { 'html': true } });
        };
        // console.log(data)
        // enable style per combination
        let chartRows = new Array();
        let chartRow = this.combinationData;
        let cv_length = this.combo2ChartViews[comboChartidx].length


        // for (let i = 0; i < 10; i++) { // for each time record
        for (let i = 0; i < rowCount; i++) { // for each time record
            for (let j = 0; j < cv_length; j++) {  //for each combination
                if (this.displayComboUnit2(comboChartidx, j)) { //if j is 0, run for all combo, if j is > 1, then run only for that combo
                    for (let k = 0; k < this.chillerCombos2.length; k++) {  //get the right combo name, handle the index diff between chartview and actual combo array
                        if (k === j - 1) {
                            let chiller_pump_cop = `${this.chillerCombos2[k]}+pump COP*`;
                            let curr_row_temp = Math.round(chartRow['Outdoor temperature'][i])
                            let target_temp_index = temperatures.indexOf(curr_row_temp)
                            // console.log(`j is ${j}`)
                            // console.log(`k is ${k}`)
                            // console.log(`i is ${i}`)
                            // console.log(`chiller_cop is ${chiller_cop}`)
                            // console.log(`curr_row_temp is ${curr_row_temp}`)
                            // console.log(`target_temp_index is ${target_temp_index}`)

                            let target = chartRow[chiller_pump_cop][i];
                            let rowData = new Array(data.getNumberOfColumns()).fill(null);
                            rowData[0] = chartRow['Main cooling load kW'][i];

                            // rebuild tooltips content, it will show point's chiller combination instead of the series (tempreature)
                            let tooltip_number = '<br>' + Math.round((chartRow['Main cooling load kW'][i]) * 100) / 100 + ', ' + Math.round(target * 100) / 100
                            // if condition to prevent replacing the rowData[0] data
                            // also assign style to the points base on chiller combination (represented by k)
                            // assign new tooltip
                            let rowData_idx: number = (target_temp_index === 0) ? 1 : (target_temp_index * 3) + 1
                            rowData[rowData_idx] = target
                            rowData[rowData_idx + 1] = this.style_map[k]
                            rowData[rowData_idx + 2] = this.chillerCombos2[k] + tooltip_number
                            chartRows.push(rowData);
                            // console.log(chartRows)
                        }
                    }
                }
            }
            // console.log(chartRows)
            // break
        }

        data.addRows(chartRows);
        // console.log(data)
        let hAxisConf = {
            title: 'Main cooling load kW',
            format: '#.#',
            titleTextStyle: {
                fontSize: 14,
                italic: false
            }
        }
        const options = this.getGoogleChartOptions(chartTitle, 'COP*'
            , true, true, 'refer to hAxisConf Object', true, hAxisConf);

        options['tooltip'] = { isHtml: true }  // required just for editing tooltip content

        let chart = new this.gLib.visualization.ScatterChart(this.combo2Chart5?.nativeElement);
        this.addChartThumbnail(this.combo2Chart5Thumb?.nativeElement, chart);
        chart.draw(data, options);
    };


    drawChart4(): void {
        this.api.logConsole('draw chart 4');
        const chartIdx = 3;

        // collect all reported dates
        let chartDates = new Array<Date>();
        for (let i = 0; i < this.chartData.length; i++) { // for each dataset
            let dataSet = this.chartData[i].data;
            let dsRows = Object.keys(dataSet['Time']).length;
            for (let j = 0; j < dsRows; j++) {
                let shortDate = dataSet['Time'][j].toString().split(' ')[0];
                if (chartDates.indexOf(shortDate) < 0) {
                    chartDates.push(shortDate);
                }
            }
        }
        chartDates.sort();
        // console.log('chart dates', chartDates);

        // setup static kW line
        const availableChillers = this.availableChillers();
        let combChillers = 617;
        let combChillers2 = 0;
        let staticLineTitle2 = '';
        if (availableChillers.length > 1 && this.chartViews[3][0]) { // all view
            combChillers2 = 617 * 2; // availableChillers.length;
            // staticLineTitle2 = `${availableChillers.length} Chillers: ${availableChillers.length} * 617 kW`;
            staticLineTitle2 = `2 Chillers: 2 * 617 kW`;
        }
        //if (availableChillers.length == 1) {
        // console.log('chartviews', this.chartViews);
        if (this.chartViews[3][0]) {
            /*
            let tmpSize = availableChillers[0].size;
            if (tmpSize != undefined) {
                console.log('tmpsize', tmpSize);
                combChillers = tmpSize;
            }
            */
        } else {
            for (let i = 1; i < this.chartViews[3].length; i++) {
                if (this.chartViews[3][i]) {
                    // console.log('selected', i);
                    let tmpSize = this.chillers[i - 1].size;
                    if (tmpSize != undefined) {
                        // console.log('tmpsize', tmpSize);
                        combChillers = tmpSize;
                    }
                }
            }
        }
        // }
        let staticLineTitle = `1 Chiller: ${combChillers} kW`;

        // setup columns
        const chartTitle = 'Daily Cooling Load';
        let data = new this.gLib.visualization.DataTable();
        data.addColumn('datetime', 'Time');
        for (let i = 0; i < chartDates.length; i++) {
            data.addColumn('number', chartDates[i]);
            data.addColumn({ type: 'string', role: 'tooltip', p: { 'html': true } });
        }
        data.addColumn('number', staticLineTitle);
        data.addColumn({ type: 'string', role: 'tooltip', p: { 'html': true } });
        if (combChillers2 != 0) {
            data.addColumn('number', staticLineTitle2);
            data.addColumn({ type: 'string', role: 'tooltip', p: { 'html': true } });
        }

        // initialize data arrays
        let rows = new Array();
        for (let j = 0; j < 96; j++) {
            let cellTime = new Date(2020, 0, 1); // use same date for date time alignment
            cellTime.setHours(0, j * 15);
            let rowData = new Array<any>((chartDates.length * 2) + 3 + (combChillers2 == 0 ? 0 : 2)); // date and tooltip, + time + static line
            rowData[0] = cellTime;
            const formattedTime = this.formatTime(cellTime);
            for (let i = 0; i < chartDates.length; i++) {
                rowData[(i * 2) + 1] = 0; // default 0 value
                rowData[(i * 2) + 2] = `${formattedTime}: 0 kW`; // formatted tooltip default
            }
            if (combChillers2 == 0) {
                rowData[rowData.length - 2] = combChillers;
                rowData[rowData.length - 1] = this.formatTimeToolTip('', formattedTime, combChillers);
            } else {
                rowData[rowData.length - 4] = combChillers;
                rowData[rowData.length - 3] = this.formatTimeToolTip('', formattedTime, combChillers);
                rowData[rowData.length - 2] = combChillers2;
                rowData[rowData.length - 1] = this.formatTimeToolTip('', formattedTime, combChillers2);
            }
            rows.push(rowData);
        }
        // console.log('init arrays', rows);

        // fill arrays
        for (let k = 0; k < availableChillers.length; k++) {
            if (this.chartViews[3][k + 1] || this.chartViews[3][0]) { // total chiller data for single or all
                // console.log('fill cd', k);
                let chillerDataSet = this.chartData[k];
                let chartName = chillerDataSet.id;
                // let chartRow = chillerDataSet.data;
                let dataSet = chillerDataSet.data;
                let dsRows = Object.keys(chillerDataSet.data['Time']).length;
                for (let i = 0; i < dsRows; i++) {
                    let timeStamp = dataSet['Time'][i];
                    let tsParts = timeStamp.split(' ');
                    let tsDate = tsParts[0];
                    let tsTime = tsParts[1];
                    let tsDateOffset = chartDates.indexOf(tsDate);
                    let tsTimeOffset = this.getTimeOffset(tsTime);
                    // let dataCell = (tsDateOffset * 96) + tsTimeOffset;
                    let cellTime = new Date(2020, 1, 1); // use same date for date time alignment
                    cellTime.setHours(0, tsTimeOffset * 15);
                    const formattedTime = this.formatTime(cellTime);
                    // let cellTotal = 0;
                    let chillerData = dataSet[`${chartName} cooling load kW (m*cp*dT)`][i];
                    let aggregateData = rows[tsTimeOffset][(tsDateOffset * 2) + 1] + chillerData;
                    rows[tsTimeOffset][(tsDateOffset * 2) + 1] = aggregateData;
                    let toolTip = this.formatTimeToolTip(tsDate, formattedTime, aggregateData);
                    rows[tsTimeOffset][(tsDateOffset * 2) + 2] = toolTip;
                    // console.log(timeStamp, tsDate, tsDateOffset, formattedTime, tsTimeOffset, aggregateData, toolTip);
                }
            }
        }
        // console.log('filled arrays', rows);
        data.addRows(rows);

        const options = this.getGoogleChartOptions(chartTitle, 'Cooling Load (kW)', false, true, 'time', false);

        let chart = new this.gLib.visualization.LineChart(this.chart4?.nativeElement);
        this.addChartThumbnail(this.chart4Thumb?.nativeElement, chart);

        this.filteredCooling = false;
        let ctx = this;
        this.gLib.visualization.events.removeListener(chart, 'select');
        this.gLib.visualization.events.addListener(chart, 'select', function () {
            let sel = chart.getSelection();
            if (sel.length > 0 && !ctx.filteredCooling) {
                if (sel[0].row == null) {
                    let col = sel[0].column;
                    let view = new ctx.gLib.visualization.DataView(data);
                    const columnCount = view.getNumberOfColumns();
                    // console.log('select', columnCount);
                    for (let i = 0; i < columnCount; i++) {
                        if (i == col) {
                            var staticLine = columnCount - 2;
                            if (combChillers2 > 0) {
                                view.setColumns([0, col, col + 1, staticLine - 2, staticLine - 1, staticLine, staticLine + 1]);
                            } else {
                                view.setColumns([0, col, col + 1, staticLine, staticLine + 1]);
                            }
                            // view.setColumns([0, staticLine, staticLine + 1]);
                            ctx.filteredCooling = true;
                        }
                    }
                    chart.draw(view, options);
                }
            }
        });
        chart.draw(data, options);
    }
    drawChart4Bkp(): void {
        this.api.logConsole('draw chart 4');
        const chartIdx = 3;

        const rowCount = Object.keys(this.chartData[0].data['Time']).length;

        // gather dates per line
        let chartDates = new Array<Date>();
        for (let i = 0; i < rowCount; i++) {
            let chartRow = this.chartData[0].data;
            let shortDate = chartRow['Time'][i].toString().split(' ')[0];
            if (chartDates.indexOf(shortDate) < 0) {
                chartDates.push(shortDate);
            }
        }
        chartDates.sort();
        // console.log('chart dates', chartDates);

        // setup static kW line
        const availableChillers = this.availableChillers();
        let combChillers = 617;
        let staticLineTitle = '617 kW';
        if (availableChillers.length > 1 && this.chartViews[3][0]) { // all view
            combChillers = 617 * availableChillers.length;
            staticLineTitle = `${availableChillers.length} * 617 kW`;
        }

        const chartTitle = 'Daily Cooling Load';
        let data = new this.gLib.visualization.DataTable();
        data.addColumn('datetime', 'Time');
        for (let i = 0; i < chartDates.length; i++) {
            data.addColumn('number', chartDates[i]);
            data.addColumn({ type: 'string', role: 'tooltip', p: { 'html': true } });
        }
        data.addColumn('number', staticLineTitle);
        data.addColumn({ type: 'string', role: 'tooltip', p: { 'html': true } });

        // initialize data arrays
        let rows = new Array();
        for (let j = 0; j < 96; j++) {
            let cellTime = new Date(2020, 0, 1); // use same date for date time alignment
            cellTime.setHours(0, j * 15);
            let rowData = new Array<any>((chartDates.length * 2) + 3); // date and tooltip, + time + static line
            rowData[0] = cellTime;
            const formattedTime = this.formatTime(cellTime);
            for (let i = 0; i < chartDates.length; i++) {
                rowData[(i * 2) + 1] = 0; // default 0 value
                rowData[(i * 2) + 2] = `${formattedTime}: 0 kW`; // formatted tooltip default
            }
            rowData[rowData.length - 2] = combChillers;
            rowData[rowData.length - 1] = this.formatTimeToolTip('', formattedTime, combChillers);
            // `${formattedTime}: <br>${combChillers} kW`; // formatted tooltip default
            rows.push(rowData);
        }
        // fill arrays
        for (let i = 0; i < rowCount; i++) {
            // console.log('check row', i);
            let timeStamp = this.chartData[0].data['Time'][i];
            // console.log('timeStamp', timeStamp);
            let tsParts = timeStamp.split(' ');
            let tsDate = tsParts[0];
            let tsTime = tsParts[1];
            let tsDateOffset = chartDates.indexOf(tsDate);
            let tsTimeOffset = this.getTimeOffset(tsTime);
            // let dataCell = (tsDateOffset * 96) + tsTimeOffset;
            let cellTime = new Date(2020, 1, 1); // use same date for date time alignment
            cellTime.setHours(0, tsTimeOffset * 15);
            const formattedTime = this.formatTime(cellTime);
            let cellTotal = 0;
            // console.log('after offsets', i);
            for (let k = 0; k < availableChillers.length; k++) {
                if (this.chartViews[3][k + 1] || this.chartViews[3][0]) { // total chiller data for single or all
                    let chartName = this.chartData[k].id;
                    let chartRow = this.chartData[k].data;
                    let chillerData = chartRow[`${chartName} cooling load kW (m*cp*dT)`][i];
                    // console.log('chiller', chartName, timeStamp, i, chillerData);
                    cellTotal += chillerData;
                }
            }
            // console.log('after all chillers', i);
            // // console.log('row stamp', tsDate, tsTime, tsDateOffset, tsTimeOffset, cellTotal);
            rows[tsTimeOffset][(tsDateOffset * 2) + 1] = cellTotal;
            let toolTip = this.formatTimeToolTip(tsDate, formattedTime, cellTotal);
            // console.log('after tooltip', i);
            //`${tsDate} ${formattedTime}: ${cellTotal} kW`;
            rows[tsTimeOffset][(tsDateOffset * 2) + 2] = toolTip;
            // console.log(timeStamp, tsDate, tsDateOffset, formattedTime, tsTimeOffset, cellTotal, toolTip);
        }
        // console.log('data arrays', rows);
        data.addRows(rows);

        const options = this.getGoogleChartOptions(chartTitle, 'Cooling Load (kW)', false, true, 'time', false);

        let chart = new this.gLib.visualization.LineChart(this.chart4?.nativeElement);
        this.addChartThumbnail(this.chart4Thumb?.nativeElement, chart);

        let ctxGlib = this.gLib;
        this.gLib.visualization.events.removeListener(chart, 'select');
        this.gLib.visualization.events.addListener(chart, 'select', function () {
            let sel = chart.getSelection();
            if (sel.length > 0) {
                if (sel[0].row == null) {
                    let col = sel[0].column;
                    let view = new ctxGlib.visualization.DataView(data);
                    const columnCount = view.getNumberOfColumns();
                    for (let i = 0; i < columnCount; i++) {
                        if (i == col) {
                            view.setColumns([0, col, col + 1]);
                        }
                    }
                    chart.draw(view, options);
                }
            }
        });
        chart.draw(data, options);
    }
    drawChart5(): void {
        this.api.logConsole('draw chart 5');
        const chartIdx = 4;

        const designFlow = 27;
        let data = new this.gLib.visualization.DataTable();
        data.addColumn('datetime', 'Time');
        data.addColumn('number', `designed flow= ${designFlow} l/s`);

        let chartTitle = 'Flowrate l/s : ';
        const chillerCount = this.chartInputs.chillers.length;
        let displayCount = 0;
        for (let i = 0; i < chillerCount; i++) {
            let chillerName = this.chartInputs.chillers[i].name;
            if (this.displayUnit(chartIdx, i)) {
                // console.log('display chart', i + 1);
                data.addColumn('number', `${chillerName} OPtimal Flowrate l/s`);
                data.addColumn('number', `${chillerName} Flowrate l/s`);
                chartTitle += (displayCount++ > 0 ? ', ' : '') + chillerName;
                // console.log('chiller cols', chillerName);
            }
        }

        let chartRows = new Array();
        const rowCount = Object.keys(this.chartData[0].data['Time']).length;
        for (let i = 0; i < rowCount; i++) {
            let cellDate = new Date(this.chartData[0].data['Time'][i]);
            let rowData = new Array();
            rowData.push(cellDate);
            rowData.push(designFlow);
            for (let j = 0; j < this.chartInputs.chillers.length; j++) {
                if (this.displayUnit(chartIdx, j)) {
                    let chillerName = this.chartInputs.chillers[j].id;
                    let chartRow = this.chartData[j].data;
                    rowData.push(chartRow[`${chillerName} OPtimal Flowrate l/s`][i]);
                    rowData.push(chartRow[`${chillerName} Flowrate l/s`][i]);
                }
            }
            chartRows.push(rowData);
        }
        // console.log('chartrows', chartRows);
        data.addRows(chartRows);

        const options = this.getGoogleChartOptions(chartTitle, 'Flow rate l/s', false, true, 'date', false);

        let chart = new this.gLib.visualization.LineChart(this.chart5?.nativeElement);
        this.addChartThumbnail(this.chart5Thumb?.nativeElement, chart);
        chart.draw(data, options);
    }
    drawChart6(): void {
        this.api.logConsole('draw chart 6');
        const chartIdx = 5;

        let data = new this.gLib.visualization.DataTable();
        data.addColumn('datetime', 'Time');

        let chartTitle = 'Electricity saving kWh : ';
        const chillerCount = this.chartInputs.chillers.length;
        let displayCount = 0;
        for (let i = 0; i < chillerCount; i++) {
            let chillerName = this.chartInputs.chillers[i].name;
            if (this.displayUnit(chartIdx, i)) {
                // console.log('display chart', i + 1);
                data.addColumn('number', `${chillerName} Electricity saving kWh`);
                chartTitle += (displayCount++ > 0 ? ', ' : '') + chillerName;
                // console.log('chiller cols', chillerName);
            }
        }

        let chartRows = new Array();
        const rowCount = Object.keys(this.chartData[0].data['Time']).length;
        for (let i = 0; i < rowCount; i++) {
            let cellDate = new Date(this.chartData[0].data['Time'][i]);
            let rowData = new Array();
            rowData.push(cellDate);
            for (let j = 0; j < this.chartInputs.chillers.length; j++) {
                if (this.displayUnit(chartIdx, j)) {
                    let chillerName = this.chartInputs.chillers[j].id;
                    let chartRow = this.chartData[j].data;
                    rowData.push(chartRow[`${chillerName} Electricity saving kWh`][i]);
                }
            }
            chartRows.push(rowData);
        }
        data.addRows(chartRows);

        const options = this.getGoogleChartOptions(chartTitle, 'Electricity saving kWh', false, true, 'date', false);

        let chart = new this.gLib.visualization.LineChart(this.chart6?.nativeElement);
        this.addChartThumbnail(this.chart6Thumb?.nativeElement, chart);
        chart.draw(data, options);
    }
    getData(): any {
        let arr = [];
        for (let i = 0; i < 10; i++) {
            arr.push(Array(10).fill(i).map(() => Math.random()))
        }
        return arr;
    }
    formatTime(rawDate: Date): string {
        const hr = rawDate.getHours();
        const min = rawDate.getMinutes();
        let formatted = (hr < 10 ? '0' : '') + hr.toString();
        formatted += (min < 10 ? ':0' : ':') + min.toString();
        formatted += (hr > 12 ? 'pm' : 'am');
        return formatted;
    }
    formatTimeToolTip(date: string, formattedTime: string, load: number): string {
        let formattedDate = date;
        const monthNames = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        if (date != '') {
            const dateParts = date.split('-');
            formattedDate = `${monthNames[Number(dateParts[1]) - 1]} ${dateParts[2]}, ${dateParts[0]}`;
        }
        const formattedDateTime = date == '' ? formattedTime : `${formattedDate}, ${formattedTime}`;
        const formattedLoad = `Cooling Load: <b>${load} kW</b>`;
        const formatted = `<b>${formattedDateTime}</b><br>${formattedLoad}`;
        return formatted;
    }
    getTimeOffset(time: string): number {
        let offset = 0;
        const timeParts = time.split(':');
        offset = (Number(timeParts[0]) * 4) + (Number(timeParts[1]) / 15 * 1);
        return offset;
    }
    toggleAnalysis(msg: string): void {
        // for (let k in this.buttons) {
        //     this.buttons[k] = !this.buttons[k]
        // }
        for (let k in this.buttons) {
            this.buttons[k] = false
        }
        // console.log(msg)
        // console.log(this.buttons)
        this.buttons[msg] = true;
        // console.log(this.buttons)

        this.displayCombo = this.buttons['displayCombo']
        this.displayCooling = this.buttons['displayCooling']
        this.displayCOP = this.buttons['displayCOP']

        this.appRef.tick();
    }
    getTimeRange(): any {
        let range = {
            min: '9999-12-31 23:59:59',
            max: '0000-01-01 00:00:00',
            minD: new Date(),
            maxD: new Date(),
            offsets: 0
        }
        return range;
    }

    // 3D chart
    draw3dChart(): void {
        this.api.logConsole('draw 3d chart');
        let chartRows = [];
        for (let i = 0; i < this.temps3d.length; i++) {
            chartRows.push(new Array(this.percs3d.length));
        }
        // console.log(chartRows);
        for (let i = 0; i < this.rows3d.length; i++) {
            let row = this.rows3d[i];
            if (row[1] == this.selectedEvap) {
                let rowPercOff = this.percs3d.indexOf(Number(row[4]));
                let rowTempOff = this.temps3d.indexOf(Number(row[0]));
                // console.log('perc temp', rowPercOff, rowTempOff, row[4], row[0]);
                chartRows[rowTempOff][rowPercOff] = Number(row[3]);
            }
        }
        const hovertemplate = 'Cooling load [%]: %{x}<br>Inlet Condenser Temperature (C°): %{y}<br>Manufacturer COP: %{z}<extra></extra>';
        let data_z1 = { z: chartRows, type: 'surface', hovertemplate: hovertemplate };

        const layout = {
            title: 'Manufacturer COP<br\><i>(color bar is manufacturer COP)</i>',
            autosize: false,
            width: 1200,
            height: 1000,
            margin: {
                l: 65,
                r: 50,
                b: 65,
                t: 90,
            },
            scene: {
                aspectratio: {
                    x: 1, y: 1, z: 1
                },
                xaxis: {
                    title: 'Cooling load [%]',
                    nticks: this.percs3d.length,
                    ticktext: this.percs3d,
                    tickvals: this.getArrayTickVals(this.percs3d)
                },
                yaxis: {
                    title: 'Inlet Condenser Temperature (C°)',
                    nticks: 10,
                    ticktext: this.getArrayTickTexts(this.temps3d),
                    tickvals: this.getArrayTickVals(this.temps3d),
                    tickfont: {
                        size: 12
                    },
                },
                zaxis: {
                    title: 'Manufacturer COP'
                }
            }
        };

        const options = {
            displaylogo: false,
            scrollZoom: false,
            editable: false,
            displayModeBar: true
        };
        Plotly.newPlot(this.chart7?.nativeElement, [data_z1], layout, options);
        this.display3DChart = true;
        this.displayLoading = false;
        this.appRef.tick();
    }
    getArrayTickTexts(arrElems: Array<number>): Array<string> {
        let tickTexts: Array<string> = [];
        for (let i = 0; i < arrElems.length; i++) {
            let elemText = arrElems[i];
            if (elemText == Math.floor(elemText)) {
                tickTexts.push(elemText.toString());
            } else {
                tickTexts.push(' ');
            }
        }
        return tickTexts;
    }
    getArrayTickVals(arrElems: Array<number>): Array<number> {
        let tickVals: Array<number> = [];
        for (let i = 0; i < arrElems.length; i++) {
            tickVals.push(i);
        }
        return tickVals;
    }
    selectEvapTemp(select: number): void {
        this.selectedEvap = select;
        this.display3DChart = false;
        this.draw3dChart();
    }

    // charting helpers
    displayUnit(chart: number, unitIdx: number): boolean {
        return (this.chartViews[chart][0] || this.chartViews[chart][unitIdx + 1]);
    }
    displayComboUnit(chart: number, unitIdx: number): boolean {
        return (this.comboChartViews[chart][0] || this.comboChartViews[chart][unitIdx]);
        // return this.comboChartViews[chart][unitIdx];
    }
    displayComboUnit2(chart: number, unitIdx: number): boolean {
        return (this.combo2ChartViews[chart][0] || this.combo2ChartViews[chart][unitIdx]);
        // return this.comboChartViews[chart][unitIdx];
    }
    getGoogleChartOptions(chartTitle: string, yAxisTitle: string,
        allowYZoom: boolean, isSideLegend: boolean, hAxisType: string, showPoints: boolean, hAxisConf_in?: object): any {
        // if hAxisConf_in is given, it will overide hAxisType

        let hAxisConf = null;
        let toolTipConf = {};

        // if hAxisConf_in is not null, then just assign it to hAxisConf,
        // otherwise use vendor's switch
        if (hAxisConf_in) {
            hAxisConf = hAxisConf_in
        }
        else {
            switch (hAxisType) {
                case 'percent':
                    hAxisConf = {
                        title: 'Load Percentage',
                        format: '#.#',
                        minValue: 0,
                        maxValue: 1,
                        titleTextStyle: {
                            fontSize: 14,
                            italic: false
                        }
                    }
                    break;
                case 'time':
                    hAxisConf = {
                        title: 'Time of day',
                        format: 'HH:mm',
                        titleTextStyle: {
                            fontSize: 14,
                            italic: false
                        }
                    }
                    toolTipConf = { isHtml: true };
                    break;
                case 'date':
                    hAxisConf = {
                        format: 'yy-MM-dd hh:mm a',
                        slantedText: true,
                        slantedTextAngle: 60,
                        textStyle: {
                            fontSize: 10
                        }
                    }
                    break;
            }
        }
        // console.log(hAxisConf)
        return {
            title: chartTitle,
            titleTextStyle: {
                fontSize: 14,
                bold: true
            },
            tooltip: toolTipConf,
            fontSize: 12,
            pointSize: (showPoints ? 3 : 0),
            legend: {
                alignment: 'center',
                position: (isSideLegend ? 'right' : 'top'),
                maxLines: 2
            },
            vAxis: {
                title: yAxisTitle,
                titleTextStyle: {
                    fontSize: 14,
                    italic: false
                }
            },
            hAxis: hAxisConf,
            width: 1200,
            height: 500,
            explorer: {
                axis: (allowYZoom ? 'both' : 'horizontal'),
                actions: ['dragToZoom', 'rightClickToReset']
            }
        };
    }
    addChartThumbnail(thumbRef: any, chart: any): void {
        this.gLib.visualization.events.addListener(chart, 'ready', function () {
            const dataImg = chart.getImageURI();
            thumbRef.innerHTML = `<img src="${dataImg}">`;
        });
    }
    toggleChartUnits(chartIdx: number, chillerIdx: number): void {
        for (let i = 0; i < this.chartViews[chartIdx].length; i++) {
            this.chartViews[chartIdx][i] = false;
        }
        this.chartViews[chartIdx][chillerIdx] = true;
        eval(`this.drawChart${chartIdx + 1}()`);
    }
    toggleComboChartUnits(chartIdx: number, comboIdx: number): void {
        // copy cat of toggleChartUnits, but accept comboIdx instead of chartIdx
        for (let i = 0; i < this.comboChartViews[chartIdx].length; i++) {
            this.comboChartViews[chartIdx][i] = false;
        }
        this.comboChartViews[chartIdx][comboIdx] = true;
        eval(`this.drawComboChart${chartIdx + 1}()`);
    }
    toggleCombo2ChartUnits(chartIdx: number, comboIdx: number): void {
        // copy cat of toggleChartUnits, but accept comboIdx instead of chartIdx
        for (let i = 0; i < this.combo2ChartViews[chartIdx].length; i++) {
            this.combo2ChartViews[chartIdx][i] = false;
        }
        this.combo2ChartViews[chartIdx][comboIdx] = true;
        eval(`this.drawCombo2Chart${chartIdx + 1}()`);
    }
    availableChillers(): Array<Chiller> {
        let available: Array<Chiller> = new Array<Chiller>();
        for (let i = 0; i < this.chillers.length; i++) {
            if (this.chillers[i].selected) {
                available.push(this.chillers[i]);
            }
        }
        return available;
    }
    availableChillersCombination(): Array<String> {
        let available = ['ACC3', 'ACC1 or ACC2', '(ACC1 or ACC2) + ACC3', 'ACC1 + ACC2', 'ACC1 + ACC2 + ACC3']
        return available;
    }
    toggleChartView(): void {
        this.displayTab = (this.displayTab == '3d' ? 'general' : '3d');
        this.appRef.tick();
    }
}
