import { Component, OnInit, NgZone } from '@angular/core';
import { Router } from '@angular/router';

// https://github.com/angular/angularfire
// import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase/app';

import { ApiService } from './../../service/api.service';

import { Config } from './../../config/config';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IBroadcastMessage } from 'src/app/model/core/broadcast-message.interface';

@Component({
    selector: 'login',
    templateUrl: 'login.component.html',
})
export class LoginComponent implements OnInit {


    constructor(
        // public readonly auth: AngularFireAuth,
        private readonly router: Router,
        private zone: NgZone,
        public readonly api: ApiService) { 
            /*
        let authState = this.auth.authState;
        authState.subscribe(user => {
            if (user)  {
                // console.log('already subscribed', user);
                this.connectHvacLogin(this.api.getFirebaseSessionToken());
            }
        });
        */
    }
    public displayPostError = false;

    ngOnInit() {
        this.api.logConsole('login page', Config.buildVersion);
        this.api.initGoogleAuth();
        this.api.listenForBroadcastMessage().subscribe(broadcast => {
            this.onBroadcastReceived(broadcast);
        });
    }
    onBroadcastReceived(broadcast: IBroadcastMessage) {
      switch (broadcast.message) {
        case Config.CastLoggedIn:
            // this.api.navigateTo('/charts', 0);
            this.zone.run(() => {
                this.router.navigate(['/charts']);
            })
        break;
      }
    }
    logout(): void {
        this.api.logout(this);
    }
    /*
    login(): void {
        this.api.login();
    }
    login(): void {
        firebase.auth().setPersistence(firebase.auth.Auth.Persistence.SESSION)
        .then(() => {
            // return firebase.auth().signInWithEmailAndPassword(email, password);
            this.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
            .then(authResult => this.onGoogleLogin(authResult));
        })
        .catch((error) => {
            // Handle Errors here.
        });
        // this.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
        // .then(authResult => this.onGoogleLogin(authResult));
        
    }
    onGoogleLogin(userCreds: any) {
        console.log('userCreds', userCreds);
        const userToken: string = userCreds.credential.idToken;
        this.api.user = userCreds.getAuthResponse(true);
        // console.log('user token', userCreds);
        this.api.saveBearerToken(userToken);
        firebase.auth().currentUser?.getIdToken().then((firebaseToken: string) => {
            // console.log('firebase token', firebaseToken);
            this.api.saveFirebaseSessionToken(firebaseToken);
            // this.connectHvacLogin(firebaseToken);
        });
    }
    connectHvacLogin(firebaseToken: any): void {
        // console.log('connect hvac', firebaseToken);
        if (Config.production) {
            if (firebaseToken != '') {
                this.api.callAPI(Config.HvacLoginActionCloud, { 'idToken': firebaseToken })
                .subscribe(
                    (response: any) => this.onHvacLogin(response, firebaseToken),
                    (error: any) => {
                        this.api.onApiError(error);
                        this.displayPostError = true;
                    }
                );
            }
        } else {
            this.goToCharts();
        }
    }
    onHvacLogin(results: any, firebaseToken: any) {
        if (this.api.apiError == null) {
            this.goToCharts();
        } else {
            this.displayPostError = false;
        }
    }
    logout(): void {
        this.api.clearTokens();
        this.auth.signOut();
    }
    goToCharts(ctx: any): void {
        ctx.api.navigateTo('/charts', 0);
    }
    */
}
