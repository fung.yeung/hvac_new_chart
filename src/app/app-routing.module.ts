import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// https://github.com/angular/angularfire
// import { AngularFireAuthGuard, hasCustomClaim, redirectUnauthorizedTo, redirectLoggedInTo } from '@angular/fire/auth-guard';

import { LoginComponent } from './views/auth/login.component'
import { ChartsComponent } from './views/chart/charts.component'

// const redirectUnauthorizedToLogin = () => redirectUnauthorizedTo(['/']);
// const redirectLoggedInToCharts = () => redirectLoggedInTo(['charts']);

const routes: Routes = [
  {
    path: 'charts',
    component: ChartsComponent
  },
  {
    path: '**',
    component: LoginComponent
  }
];
/*

  { 
    path: '**', 
    component: LoginComponent ,
    canActivate: [AngularFireAuthGuard], 
    data: { 
      authGuardPipe: redirectLoggedInToCharts 
    }
  }
  */

@NgModule({
  imports: [
    RouterModule.forRoot(routes, 
      { onSameUrlNavigation: 'reload', 
      scrollPositionRestoration: 'top' 
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }

